#!/usr/bin/env python
# -*- encoding: utf-8 -*-
import os, cgi, cups, json

print "Content-Type: text/html"
print "Access-Control-Allow-Origin: *"
print "Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept"
print "<html>"
print

conn = cups.Connection()
printers = conn.getPrinters()
default = conn.getDefault()
form = cgi.FieldStorage(keep_blank_values=1)

desarrollo = False;
accion = 'visualizar' # listar, estatus, descargar, verificar, imprimir, visualizar
clave = '123456'
tienda = 'T000'
ip = '192.168.66.31'
url = 'http://127.0.0.1:2002'

if desarrollo != True:
	accion = form["accion"].value
	clave = form["clave"].value
	tienda = form["tienda"].value
	ip = form["ip"].value
	url = form["url"].value
#------------------------------------------------------------LISTAR------------------------------------#
### {
if accion == "listar":
	if desarrollo == True:
		nombre_impresora_clave = 'PDF'
		palabra_clave1 = ''
		palabra_clave2 = ''
	else:
		nombre_impresora_clave = form["nombre_impresora_clave"].value
		palabra_clave1 = form["palabra_clave1"].value
		palabra_clave2 = form["palabra_clave2"].value

	datos = '{'
	for printer in printers:
		if nombre_impresora_clave in printer:
			try:
				if palabra_clave1 in printers[printer]["printer-location"]:
					try:
						if palabra_clave2 in printers[printer]["device-uri"]:
							if default == printer:
								impresora = printer+' (por defecto)'
							else:
								impresora = printer
							if 'usb' in printers[printer]["device-uri"]:
								impresora = impresora+' (USB)'

							imp = impresora.strip()
							datos += '"' + printer + '":[{'
							datos += '"info":"' + printers[printer]["printer-info"].strip() + '",'
							datos += '"uri":"' + printers[printer]["device-uri"].strip() + '",'
							datos += '"location":"' + printers[printer]["printer-location"].strip() + '"}],'
					except:
						print ''
			except:
				print ''
	datos = datos[:-1]
	datos += '}'
	print datos
### }
#------------------------------------------------------------IMPRIMIR------------------------------------#
### {
elif accion == "imprimir":
	if desarrollo == True:
		impresora = 'PDF'
		device_uri = "cups-pdf:/ "
		documento_nombre = 'test'
	else:
		impresora = form["impresora"].value
		device_uri = form["device_uri"].value
		documento_nombre = form["documento_nombre"].value

	archivo = "/tmp/"+documento_nombre+".pdf"
	
	try:
		printer_returns = conn.printFile(impresora, archivo, documento_nombre+" ARAUCA", {'Copies': '1'})
		print 'archivo: %d' % printer_returns
	except cups.IPPError as (status, description):
		print 'error: %d' % status
		#print 'Meaning:', description

	'''
	codigo_error:1042
	Meaning: No such file or directory
	codigo_error:1030
	Meaning: La impresora o clase no existe.
	'''
### }
#------------------------------------------------------------VISUALIZAR------------------------------------#
### {
elif accion == "visualizar":
	if desarrollo == True:
		impresora = "PDF"
		device_uri = "cups-pdf:/ "
		documento_id = 0
		documento_nombre = "Cierre_de_Caja"
		documento_tipo = "PDF"
	else:
		impresora = form["impresora"].value
		device_uri = form["device_uri"].value
		documento_id = form["documento_id"].value
		documento_nombre = form["documento_nombre"].value
		documento_tipo = form["documento_tipo"].value

	if documento_tipo == "PDF":
		tip_d = "pdf"
	elif documento_tipo == "DOC":
		tip_d = "pdf"
	else:
		tip_d = "xlsx"

	os.system("wmctrl -l | grep '"+documento_nombre+"."+tip_d+"' | wc -l > /tmp/.s"+tip_d+"")
	fich=open("/tmp/.s"+tip_d+"",'r')
	texto=fich.read() 
	lista=texto.split('\n')
	fich.close() 
	os.system('rm -f /tmp/.s'+tip_d+'')	
	if lista[0]=="0":
		if documento_tipo =="PDF":
			os.system("evince "+'/tmp/'+documento_nombre+"."+tip_d+' &')
		else:		
			os.system("soffice --calc "+'/tmp/'+documento_nombre+"."+tip_d+' &')
	else:
		os.system("wmctrl -a '"+documento_nombre+"."+tip_d+"';")
### }
#------------------------------------------------------------ESTATUS------------------------------------#
### {
elif accion == "estatus":
	var = 0
	if desarrollo == True:
		impresora = 'PDF'
		device_uri = "cups-pdf:/ "
		documento_id = 248
	else:
		impresora = form["impresora"].value
		device_uri = form["device_uri"].value
		documento_id = form["documento_id"].value
		
	'''try:
		printer_returns = conn.printFile('PDF', '/home/jesus/Documentos/clasif-presup-2014.pdf', 'FACTURA ARAUCA IMPRESA', {'Copies': '9000'})
		print printer_returns
	except cups.IPPError as (status, description):
		print 'codigo_error:%d' % status
		print 'Meaning:', description
	try:
		printer_returns = conn.printFile('PDF', '/home/jesus/Documentos/clasif-presup-2014.pdf', 'FACTURA ARAUCA IMPRESA', {'Copies': '9000'})
		print printer_returns
	except cups.IPPError as (status, description):
		print 'codigo_error:%d' % status
		print 'Meaning:', description'''

	for printer in printers:
		if impresora.strip() == printer.strip():
			if device_uri.strip() == printers[printer]["device-uri"].strip():
				try:
					salida = "/tmp/salida_impresora"
					cola = "lpstat -o -p '"+impresora+"' "
					cola = cola+"| grep -v 'activ' | grep -v 'failed' | grep -v 'cancel' | "
					if documento_id != 0:
						cola = cola+"grep '" + str(documento_id) + "' | "
					cola = cola+"grep -v 'Enviand' | wc -l > "+salida+"3"
					os.system(cola)
					f = open(salida+"3",'r')
					os.system("rm -f "+salida+"3")
					cola = f.read()
					cola = cola.split('\n')[0]
					f.close()
					var = 1
					print cola
				except:
					print 'error_desconocido'
			else:
				print 'error_device-uri'
	if var == 0:
		print 'error_configuracion'
### }
#------------------------------------------------------------DESCARGAR------------------------------------#
### {documento_nombre, documento_tipo
elif accion == "descargar":
	if desarrollo == True:
		impresora = 'PDF'
		device_uri = "cups-pdf:/ "
		documento_id = "13882"
		documento_tipo = "DOC"
		documento_nombre = "COT-000...010017110"
		tipo_impresion = "COT"
		controlador = "192.168.66.31"
		filtros = "0"
	else:
		impresora = form["impresora"].value
		device_uri = form["device_uri"].value
		documento_id = form["documento_id"].value
		documento_tipo = form["documento_tipo"].value
		documento_nombre = form["documento_nombre"].value
		tipo_impresion = form["tipo_impresion"].value
		controlador = form["controlador"].value
		filtros = form["filtros"].value
	salida = "/tmp/salida_descarga"
	configuracion = "tienda_-_"+tienda+"_._clave_-_"+clave+"_._ip_-_"+ip

	if documento_tipo == "PDF":
		tip_d = "pdf"	
	elif documento_tipo == "DOC":
		tip_d = "pdf"
	else:
		tip_d = "xlsx"

	if tipo_impresion != "REP":
		configuracion += "_._tipo_-_"+tip_d
	else:
		configuracion += "_._tipo_-_"+tipo_impresion

	variables = configuracion+"_union_"+filtros
	variables = variables.replace(' ','_esp_').replace(':','_dosp_')
	archivo = "/tmp/"+documento_nombre+"."+tip_d
	os.system("wmctrl -l | grep '"+documento_nombre+"."+tip_d+"' | wc -l > /tmp/.s"+tip_d+"")	
	fich = open("/tmp/.s"+tip_d+"",'r')
	texto = fich.read() 
	lista = texto.split('\n')
	fich.close() 
	os.system('rm -f /tmp/.s'+tip_d+'')

	if documento_tipo != "DOC":
		if lista[0]=="0":
			comando = "echo '' > "+salida+";wget -o "+salida+" -O "+archivo+" "+controlador+"/"+variables
			#comando = "echo '' > "+salida+";wget -o "+salida+" -O "+archivo+" "+url+"/Navegador/imprimir?valores="+variables
		else:
			os.system("wmctrl -a '"+documento_nombre+"."+tip_d+"';")
		#http://192.168.66.31:2002/Navegador/imprimir?valores=tienda_-_T000_._clave_-_123456_._ip_-_192.168.66.132_._id_-_178_._tipo_-_pdf
		#comando = "echo '' > /tmp/salida_descarga;wget -o /tmp/salida_descarga -O /tmp/FAC-001000000000000000128.pdf http://w3.id.tue.nl/fileadmin/id/objects/E-Atelier/Phidgets/Software/Flash/fl8_tutorials.pdf &"
		#os.system(comando)
	else:	
		comando = "echo '' > "+salida+";wget -o "+salida+" -O "+archivo+" "+url+"/Navegador/imprimir?valores="+configuracion+"_._id_-_"+documento_id
	os.system(comando)
	print 1
### }
#------------------------------------------------------------VERIFICAR------------------------------------#
### {
elif accion == "verificar":
	salida = "/tmp/salida_descarga"
	f = open(salida,'r')
	t = f.read() 
	a = t.split('\n')
	f.close() 

	f = file(salida,"r")
	l = f.readlines()
	f.close()
	cadena = l[-2][62:-1]

	if cadena.find("guardado") >= 0:
		cade = "Estado: Listo"
	elif cadena.find("100%") >= 0:
		cade = "Estado: Listo"
	elif t.find("Grabando a:") >= 0:	
		cade = "Estado: Descargando"
	elif t.find("esperando respuesta") >= 0:
		cade = "Estado: Esperando Respuesta"
	elif t.find("Conectando con") >= 0:		
		cade = "Estado: Conectando"
	elif t.find("--  http") >= 0:		
		cade = "Estado: Conectando"
	print cade

	if t.find("Grabando a:") >= 0:	
		cade = "Total: "+cadena
		if cade.find("guardado") >= 0:
			cade = "Total: 100%"
	else:
		cade = "Total: 0%"		

	if cade == "Total: ":
		cade = "Total: 0%"
	print cade

	if t.find("Longitud:") >= 0:
		cade = "Peso:"+t.split('Longitud:')[1].split('[')[0]
		if cade != "Peso: no especificado ":
			cade = "Peso: "+t.split('Longitud:')[1].split('[')[0].split('(')[1].split(')')[0]
		else:
			if t.find("Grabando a:") >= 0:
				peso = t.split('Grabando a:')[1].split('[')[0].split('/tmp/')[1].split('.')[0]
				os.system("du -h /tmp/"+peso+".* > "+salida+".;")
				f = open(salida+".",'r')
				t = f.read() 
				a = t.split('/tmp/')[0].strip()
				f.close()
				os.system("rm -f /tmp/"+salida+".;")
				cade = "Peso: "+a

	else:
		cade = "Peso: Calculando..."
	print cade	
	os.system("sleep 1;")	
### }
else:
	print 'NO EXISTE LA ACCION: ' + accion
